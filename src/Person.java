
public class Person {

	private int perMinWalk;
	private int perMinRun;
	private String name;

	public Person(int perMinWalk, int perMinRun, String name){
		this.perMinWalk = perMinWalk;
		this.perMinRun = perMinRun;
		this.name = name;
	}

	public Object walk(int min) {
		return perMinWalk*min;
	}
	public int run(int min) {
		throw new ArithmeticException();
	}

}
